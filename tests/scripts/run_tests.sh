#!/usr/bin/bash

if rpm -q --quiet libtraceevent; then
    :
else
    sudo dnf install -y libtraceevent
    if [[ $? != 0 ]]; then
       echo "install of libtraceevent failed!"
       exit 1
    fi
fi

echo "libtraceevent is a library to parse raw trace event formats. Check installation."
if [[ ! -f /usr/lib64/libtraceevent.so.1 ]]; then
    echo "/usr/lib64/libtraceevent.so.1 not found!"
    exit 2
fi

echo "Check the trace-cmd works."
if ! rpm -q --quiet trace-cmd; then
    sudo dnf install -y trace-cmd
    if [[ $? != 0 ]]; then
        echo "install trace-cmd failed when libtraceevent exist!"
        exit 3
    fi
fi
trace-cmd list || exit 4

exit 0

